#+TITLE:     explanations.org
#+AUTHOR:    Jannis Uhlendorf
#+EMAIL:     uhlendorf@oxymoron.biologie.hu-berlin.de${HOSTNAME}
#+DATE:      2014-01-10 Fri
#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:
#+LINK_HOME:
#+XSLT:


* Steady State
A steady state denotes a state in which metabolite concentrations do not change over time.

Formula:

$\frac {d} {dt}s(t) = Nv(s(t),p,t) = 0$

* $\pi$-elasticities
The parameter- or $\pi$-elasticities are the partial derivatives of the reaction velocities $v(s(t),p,t)$ with respect to the parameters.
Elasticities describe the direct influence a parameter (or substrate in case of $\epsilon$-elasticities) has on a reaction rate. In
contrast to control or response coefficients, they are local coefficients, meaning they do not take into account structural information.

Formulas:

$\pi_i^k = \frac {\partial v_k(s(t),p,t)} {\partial p_m}$  (unscaled)

$\tilde{\pi}_i^k = \frac{p_m} {v_k} \frac {\partial v_k(s(t),p,t)} {\partial p_m}  = \frac {\partial ln v_k(s(t),p,t)} {\partial ln p_m}$  (scaled)

* $\epsilon$-elasticities
The substrate- or $\epsilon$-elasticities are the partial derivatives of the reaction velocities ($v(s(t),p,t)$) with respect to substrate
concentrations. Elasticities describe the direct influence a substrate (or parameter in case of $\pi$-elasticities) has on a reaction rate.
In contrast to control or response coefficients, they are local coefficients, meaning they do not take into account structural information.

Formulas:

$\epsilon_i^k = \frac {\partial v_k(s(t),p,t)} {\partial s_i}$  (unscaled)

$\tilde{\epsilon}_i^k = \frac{s_i} {v_k} \frac {\partial v_k(s(t),p,t)} {\partial s_i}  = \frac {\partial ln v_k(s(t),p,t)} {\partial ln s_i}$ (scaled)


* Flux control coefficients
Flux control coefficients describe the effect that a small change of a reaction rate has on the steady state reaction rates (fluxes). In contrast
to elasticities, which consider each reaction isolated and are thereby local coefficients, control coefficients also take into account the structure
of the reaction network and assume a steady state. Varying one reaction rate will therefore also influence other reactions and metabolite concentrations
in order to fulfill the steady state condition. Therefore control coefficients are called global coefficients.

Formulas:

$^{J}C_k^j = \frac {\partial j_j} {\partial v_k}$ (unscaled)

$^{J}\tilde{C}_k^j = \frac {v_k} {j_j} \frac {\partial j_j} {\partial v_k} = \frac {\partial ln j_j} {\partial ln v_k}$ (unscaled)

Computation:

$^{J}C = I - \frac {\partial v} {\partial s} \left( N \frac {\partial v} {\partial s} \right)^{-1} N = I + \frac {\partial v} {\partial s} ^SC$

or more precisely:

$^{J}C = I - \frac {\partial v} {\partial s} L \left( N \frac {\partial v} {\partial s} \right)^{-1} N_R$


* Concentration control coefficients
Concentration control coefficients describe the effect that a small change of a reaction rate has on the steady state metabolite concentrations. In contrast
to elasticities, which consider each reaction isolated and are thereby local coefficients, control coefficients also take into account the structure
of the reaction network and assume a steady state. Varying one reaction rate will therefore also influence other reactions and metabolite concentrations
in order to fulfill the steady state condition. Therefore control coefficients are called global coefficients.

Formulas:

$^{S}C_k^i = \frac {\partial s_i} {\partial v_k}$ (unscaled)

$^{S}\tilde{C}_k^i = \frac {v_k} {s_i} \frac {\partial s_i} {\partial v_k} = \frac {\partial ln s_i} {\partial ln v_k}$ (scaled)

Computation:

$^{S}C = -(N \frac {\partial v} {\partial s})^{-1}N$

or more precisely:

$^{S}C = -L(N \frac {\partial v} {\partial s})^{-1}N_R$


* Flux response coefficients
Flux response coefficients describe the effect that a small change of a parameter has on the steady state reaction rates (fluxes). In contrast
to elasticities, which consider each reaction isolated and are thereby local coefficients, response coefficients also take into account the structure
of the reaction network and assume a steady state. Varying one parameter will therefore influence other reactions and metabolite concentrations
in order to fulfill the steady state condition. Therefore response coefficients are called global coefficients.

Formulas:

$^JR_m^j = \frac {\partial j_j} {\partial p_m}$ (unscaled)

$^J\tilde{R}_m^j = \frac {p_m} {j_j} \frac {\partial j_j} {\partial p_m} = \frac {\partial ln j_j} {\partial ln p_m}$ (scaled)

Computation:

$^JR = [I - \frac {\partial v} {\partial s} \left( N \frac {\partial v} {\partial s} \right)^{-1}N] \frac {\partial v} {\partial p}$

or more precisely:

$^JR = [I - \frac {\partial v} {\partial s} L \left( N \frac {\partial v} {\partial s} \right)^{-1}N_R] \frac {\partial v} {\partial p}$


* Concentration response coefficients
Concentration response coefficients describe the effect that a small change of a parameter has on the steady state metabolite concentrations. In contrast
to elasticities, which consider each reaction isolated and are thereby local coefficients, response coefficients also take into account the structure
of the reaction network and assume a steady state. Varying one parameter will therefore influence other reactions and metabolite concentrations
in order to fulfill the steady state condition. Therefore response coefficients are called global coefficients.

Formulas:

$^SR_m^i = \frac {\partial s_i} {\partial p_m}$ (unscaled)

$^S\tilde{R}_m^i = \frac {p_m} {s_i} \frac {\partial s_i} {\partial p_m} = \frac {\partial ln s_i} {\partial ln p_m}$ (scaled)

Computation:

$^SR = -\left( N \frac {\partial v} {\partial s} \right) N \frac {\partial v} {\partial p}$

or more precisely:

$^SR = -L \left( N \frac {\partial v} {\partial s} \right) N_R \frac {\partial v} {\partial p}$

* Time dependent concentration response coefficients
Time dependent concentration response coefficients describe how the influence, a parameter has on a substrate concentration, changes over time.
Their definition is analogous to the steady state concentration response coefficients and the time varying coefficients converge to the fixed
coefficients.

Formulas:

$^SR_m^i = \frac {\partial s_i} {\partial p_m} (t)$ (unscaled)

$^SR_m^i = \frac {p_m} {s_i(t)} \frac {\partial s_i} {\partial p_m} (t) =  \frac {\partial ln s_i} {\partial ln p_m} (t)$ (scaled)

Computation:

$\frac {d} {dt} \frac {\partial s(t)} {\partial p} = N \left( \frac {\partial v(t)} {\partial s} \frac {\partial s(t)} {\partial p} + \frac {\partial v(t)} {\partial p} \right)$


* Symbols
| $t$           | time                               |
| $s(t)$        | vector of substrate concentrations |
| $N$           | stoichiometric matrix              |
| $p$           | parameter vector                   |
| $v(s(t),p,t)$ | vector of reaction velocities\\    |
| $j$           | steady state flux                  |
| $I$           | identity matrix                   |







