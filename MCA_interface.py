#!/usr/bin/env python
import sys
sys.path.append('./MCA')
sys.path.append('./sbml-mca')
import MCA, sbml_mca, libsbml

class MCA_interface:
    """ Interface class for MCA methods (MCA.py and sbml-mca.py) """

    def __init__( self, model ):
        """ @type model: string
            @param model: SBML model as string """
        doc = libsbml.readSBMLFromString( model )
        model = doc.getModel()
        self._sbml_mca = sbml_mca.sbml_mca( model )

    def get_flux_cc( self ):
        """ get flux control coefficients
        @rtype: numpy.ndarray
        @return: matrix of flux control coefficients
        """
        return self._sbml_mca.get_flux_cc( string_output=False )




if __name__ == '__main__':
    if len(sys.argv)>1:
        #m = MCA.mca_model( sys.argv[1] )
        #m.time_dep_resp_coeff( 200, .1 )
        #m.plot_resp_coeff( 200 )

        #f = open( sys.argv[1], 'r' )
        #m = MCA_interface( f.read() )
        #print type( m.get_flux_cc() )

        m = sbml_mca.sbml_mca( sys.argv[1] )
        p = m._get_parameter_ids()
        #print m.get_elasticities_symbolic( p )

        #import time
        #t = time.time()
        sym = m.get_time_varying_response_coeff( 200 )
        #print 'symbolic ', time.time()-t
        #t = time.time()
        #sym2 = m.get_time_varying_response_coeff2( 20 )
        #print 'symbolic2 ', time.time()-t
        #t = time.time()
        num = m.get_time_varying_response_coeff_numerical( 200 )
        #print 'numerical ', time.time()-t

        import numpy, pylab
        #t = numpy.linspace(0,20,100)
        #pylab.plot( t, sym )
        #pylab.show()
        #pylab.plot( t, num )
        #pylab.show()
        #print sym-num

        print
        print
        rs = sym[-1].reshape( len(m._species_ids), len(p) )
        print rs

        #print sym2[-1].reshape( len(m._species_ids), len(p) )
        print 'results'
        print
        rn = num[-1].reshape( len(m._species_ids), len(p) )
        print rn
        print
        print
        rf = m.get_conc_resp( string_output=False )
        print rf
        print numpy.linalg.norm( sym-num )
        print numpy.linalg.norm( rf - rs )
        print numpy.linalg.norm( rf - rn )
