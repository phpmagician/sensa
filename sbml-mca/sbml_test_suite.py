#!/usr/bin/env python
import sys, os, pandas, numpy
import sbml_mca

class sbml_test_suite:
    """
    class for running the SBML test suite (sbml.org/Software/SBML_Test_Suite)

    date:   04. July 2014
    author: Jannis Uhlendorf
    """

    _TRESHOLD = 1e-4 # maximal allowed distance for a trace ( for each variable )

    def __init__(self, path, writer=sys.stdout):
        """
        class constructor
        @param path: path to the SBML test suite (the directory containing the cases directory)
        @param writer: (optional) object implementing writer (default  sys.stdout)
        """
        self._path    =  os.path.join( path + '/cases/semantic' )
        self._writer  =  writer

    def run_test( self, test_no = None ):
        """
        run either a single or all tests.
        if test_no is specified, only this test is done, if test_no is empy, all tests are done
        @param test_no: integer specifiyng which test to run
        """
        no_passed = 0
        # generate list of test case numbers
        if test_no != None:
            tests = [test_no]
        else:
            tests = range( 1, 1196 )

        for number in tests:
            self._print( 'Running test no. ' + str(number) )
            # generate SBML path
            #f_path             = self._generate_file_path( number, '-sbml-l2v3.xml' )
            f_path             = self._generate_file_path( number, '-sbml-l3v1.xml' )
            try:
            #if 1:
                mca                = sbml_mca.sbml_mca( f_path )
                settings           = self._read_settings( number )
                reference_result   = self._read_results( number )

                d = self._run_single_test( mca, settings, reference_result )
                if d > self._TRESHOLD * len(settings['variables']):
                    self._print( '\t FAILED with simulation distance %e' %d )
                else:
                    no_passed += 1
                    self._print( '\t OK - passed with simulation distance %e' %d )
            except Exception, e:
                self._print( '\t FAILED with following error:' )
                self._print( '\t' + str(type(e)) + ': ' + e.message )
                #sdf

        self._print( '\n\n*** Finished testing ***')
        self._print( '\n %d of %d tests passed' %(no_passed, len(tests)))


    def _run_single_test( self, mca_obj, settings, reference_result ):
        """
        run a single test
        @param mca_obj: sbml_mca object of the model to compare
        @param settings: settigns dictionary as returned by _read_settings fct.
        @param reference_results: result dictionary as returned by _compare_results fct.
        """
        variables = mca_obj._species_ids + [s.getId() for s in mca_obj._get_constant_species()]
        # simlate model with given settings
        time, t_courses = mca_obj.integrate_with_constant_species( settings['duration'],
                                                                   settings['steps']+1,
                                                                   r_tol=settings['relative'],
                                                                   a_tol=settings['absolute'] )
        simulation_result = { 'time': time }
        for pos,v in enumerate(variables):
            simulation_result[v] = t_courses[:,pos]
        return self._compare_results( reference_result, simulation_result )

    def _print( self, msg ):
        """ print a message using the classes writer object """
        self._writer.write( str(msg) + '\n' )

    def _generate_file_path( self, number, ending ):
        number_string = '%05d' %number
        return os.path.join( self._path, number_string, number_string + ending )

    def _compare_results( self, result_a, result_b ):
        diff = 0.
        for key in result_a:
            #print key
            #print result_a[key]
            #print result_b[key]
            #print
            if key=='time':
                continue
            d = numpy.linalg.norm( result_a[key] - result_b[key] )
            diff += d
        return diff


    def _read_settings( self, number ):
        number_items = ['start', 'duration', 'steps', 'absolute', 'relative']
        list_items   = ['variables', 'amount', 'concentration']
        result = {}
        f_path = self._generate_file_path( number, '-settings.txt' )
        f = open( f_path, 'r' )
        for line in f.readlines():
            try:
                key, data = line.split(': ')
            except:
                pass
                #print line

            if key in number_items:
                result[key] = float(data)
            elif key in list_items:
                try:
                    result[key] = [ x.strip() for x  in data.split(', ' ) ]
                except:
                    result[key] = data.strip()
            else:
                result[key] = data
        f.close()
        return result

    def _read_results( self, number ):
        f_path = self._generate_file_path( number, '-results.csv' )
        result = {}
        csv_result = pandas.read_csv( f_path )
        variables = csv_result.axes[1]
        for v in variables:
            result[v] = numpy.array( csv_result[v].tolist() )
        return result


if __name__=='__main__':

    if len(sys.argv)<2:
        path = '../../sbml_test_cases'
    else:
        path = sys.argv[1]
    sts = sbml_test_suite( path )
    sts.run_test( 839 )
