Template.lineplot.events({
    //starts a simulation on click for a given time
    'click .simulate': function (event, template) {
        //set button to loading state, is reset when graph is redrawn
        $(event.target).button('loading');
        $('#input_time').prop('disabled', true);
        var selected_model = Session.get("selected_model");
        var selected_analysis = Session.get("selected_analysis");
        var time = template.find('.time').value || 1;
        Session.set("time", time);
        //call different functions depending on analysis
        if (selected_analysis === "get_time_varying_conc_rc") {
            Meteor.call('mca', 
                        selected_model,
                        "get_time_varying_conc_rc",
                        {end_time: time}); //compute it
        }
        else {
            Meteor.call('mca',
                        selected_model,
                        "get_time_course",
                        {end_time: time}); //compute it
        }
    },
    // update value of time
    'keyup .time': function (event, template) {
        var time = template.find('.time').value;
        if(Session.equals("time",time) || time === '')
            $("#btn_simulate").prop('disabled',true);
        else
            $("#btn_simulate").button('reset');
    },
    //toggles between normalized and non-normalized data
    'click #btn_normalize': function (event) {
        Session.set("normalization", true);
    },
    'click #btn_non_normalize': function (event) {
        Session.set("normalization", false);
    }
});

Template.lineplot.title = function() {
    if (Session.get('selected_analysis') == 'get_time_varying_conc_rc') {
        var title = 'Time dependent response coefficients';
    }
    if (Session.get('selected_analysis') == 'get_model_details') {
        var title = 'Time course simulation';
    }
    return title
};

Template.lineplot.normalization = function() {
    return Session.get('selected_analysis') === 'get_time_varying_conc_rc';
};
Template.lineplot.rendered = function () {
    $('.nav-tabs').button()
    var self = this;

    if (Session.get("normalization")) { 
        $('#btn_normalize').addClass("active");
    } else { 
        $('#btn_non_normalize').addClass("active");
    }

    if (! self.handle) {
        var selected_data,
            selected_coeffs;

        if (selected_analysis === "get_time_varying_conc_rc") {
            selected_data = Session.get("selected_data"); // data that should be plotted
            selected_coeffs = Session.get("selected_coeffs"); // x and y labels of the plotted data
        }
        else {
            selected_data = [];
            selected_coeffs = [];
        }
        var selected_model = Session.get('selected_model'),
            selected_analysis = Session.get('selected_analysis');
        self.handle = Deps.autorun(function () {
            var selected_model = Session.get('selected_model'),
                selected_analysis = Session.get('selected_analysis');

            //specify the fields we actually need to fetch
            var fields =  {};
            if (Session.get("normalization") && 
                selected_analysis !== "get_model_details") { // false per default, hence here normalized data
                fields[selected_analysis + ".data_normalize"] = 1;
            }
            else {
                fields[selected_analysis + ".data"] = 1;
            }
            fields[selected_analysis + ".x_axis"] = 1;
            fields[selected_analysis + ".y_axis"] = 1;
            fields[selected_analysis + ".x_axis_alt"] = 1;
            fields[selected_analysis + ".y_axis_alt"] = 1;
            fields[selected_analysis + ".time"] = 1;


            var current_analysis = Analysis.findOne({model_id: selected_model},
                                                    {fields: fields});
            var data = [];
            d3.select("#plot").selectAll("g").remove();
            d3.select("#export_data").selectAll("a").remove();
            d3.select("#selectionMatrix").selectAll("g").remove();
            d3.selectAll(".tooltip").remove();

            if(current_analysis) {
                try{
                    if (Session.get("normalization") &&
                        selected_analysis !== "get_model_details") { // false per default, hence here normalized data
                        data = current_analysis[selected_analysis].data_normalize;

                    } else { // true per default, hence here non-normalized data
                        data = current_analysis[selected_analysis].data;
                    }

                    // data = current_analysis[selected_analysis].data;
                    // TODO: labels are in the wrong orientation. fix in the relevant code instead of just switching
                    var x_lab, y_lab;
                    if(current_analysis[selected_analysis].x_axis_alt != undefined) {
                        y_lab = current_analysis[selected_analysis].x_axis_alt;
                    }
                    else{
                        y_lab = current_analysis[selected_analysis].x_axis;
                    }
                    if(current_analysis[selected_analysis].y_axis_alt != undefined) {
                        x_lab = current_analysis[selected_analysis].y_axis_alt;
                    }
                    else{
                        x_lab = current_analysis[selected_analysis].y_axis;
                    }
                    var time =  current_analysis[selected_analysis].time;

                    // generate an object to map data to x and y labels
                    var data_obj = {};
                    var k = 0;
                    for (var i=0;i<x_lab.length;i++) {
                        for (var j=0;j<y_lab.length;j++) {
                            data_obj[k] = data[k];
                            k++;
                        }
                    }

                    // test
                    selected_data =  _.map(selected_coeffs, function(x) { return data_obj[x];});
                    
                    var n = data.length,
                        m = data[0].length;

                }
                catch(e){
                    data = [];
                }

                if(data.length > 0) {
                    //plot the matrix for selection of coeff:display

                    //set the matrix to a suitable size
                    var max_width = 720,
                        max_height = 720,
                        n_rows = y_lab.length,
                        m_cols = x_lab.length;

                    // calc margin.top and .left according to max length of labels
                    var max_len_x = d3.max(x_lab.map(function(string) {
                        return string.length;
                    }));
                    var max_len_y = d3.max(y_lab.map(function(string) {
                        return string.length;
                    }));

                    var margin = {top: Math.max(80, max_len_x*9), right: 0, bottom: 10, left: Math.max(80, max_len_y*9)};
                    // var margin = {top: 150, right: 0, bottom: 10, left: 150};
                    var width, height;
                    if (max_width/m_cols < 20 || max_height/n_rows < 20) {
                        width = Math.min(max_width*(m_cols/n_rows), max_width);
                        height = Math.min(max_height*(n_rows/m_cols), max_height);
                    }
                    else {
                        width = Math.min(max_width, 20*m_cols);
                        height = Math.min(max_height, 20*n_rows);
                    }
                    //scales
                    var x = d3.scale.ordinal().rangeBands([0, width]),
                        y = d3.scale.ordinal().rangeBands([0, height]);
                    
                    var svg = d3.select("#selectionMatrix")
                            .attr("width", width + 1 + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .style("display", "inline")
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    var matrix = [];
                    k = 0
                    y_lab.forEach(function(row,i) {
                        matrix[i] = d3.range(m_cols).map(function(j) {
                            return {x: j,
                               y: i,
                               z: k++ };
                        });
                    });

                    // Precompute the orders.
                    var xOrders = {
                        // name: d3.range(m).sort(function(a, b) { return (a,b); }),
                        name: d3.range(m_cols)
                    };
                    var yOrders = {
                        // name: d3.range(n).sort(function(a, b) { return (a,b); }),
                        name: d3.range(n_rows)
                    };

                    // The default sort order.
                    x.domain(xOrders.name);
                    y.domain(yOrders.name);

                    svg.append("rect")
                        .attr("class", "background")
                        .attr("width", width)
                        .attr("height", height)
                        .style("stroke-width", 0);

                    var color = d3.scale.category20();

                    function create_row(row) {
                        var circle = d3.select(this).selectAll(".circle")
                                .data(row);
                        circle.enter().append("circle")
                            .attr("class", "circle")
                            .attr("cx", function(d) { return x(d.x)+x.rangeBand()/2; })
                            .attr("cy", x.rangeBand()/2)
                            .attr("r", 6)
                            .style("stroke", "lightgray")
                            .style("stroke-width", 2)
                            //.style("fill-opacity", 0)
                            .style("fill", function(d,i) {return color(data_obj[d.z]);})
                            .on("mouseover", mouseover)
                            .on("mouseout", mouseout)
                            .on("click", selectData);
                        circle.exit().remove();
                    }

                    function refresh_link () {
                        var html = d3.select("#plot")
                                .attr("version", 1.1)
                                .attr("xmlns", "http://www.w3.org/2000/svg")
                                .node().outerHTML;
                        d3.select("#download_link")
                            .attr("href", "data:image/svg+xml;base64,\n" + btoa(html))
                            .attr("download", selected_analysis+"_normalization_"+Session.get("normalization")+".svg");
                    }
                    
                    function refreshGraph(newData) {
                        //refresh selection matrix
                        d3.selectAll("circle").classed("active", function(d) { return _.contains(selected_coeffs, d.z); });

                        // refresh left yAxis
                        y_lp.domain(range(selected_data))
                            .range([h, 0]);
                        graph.selectAll("g.y").transition().call(yAxisLeft);

                        //add lines
                        if(newData)
                            graph.selectAll("path.line").remove();

                        var lines = graph.selectAll("path.line")
                                .data(selected_data);

                        lines.enter()
                            .append("path")
                            .attr("class", "line")
                            .attr("d", line)
                            .style("fill", "none")
                            .style("stroke-width", "1.5")
                            .style("stroke", function(d,i) { return color(d);});

                        //remove and update existing lines
                        lines.exit().remove();
                        lines.transition().attr("d",line);
                        refresh_link();
                    }

                    // get the maxmimum and minimum of a nested array
                    function range( data ){
                        var inner_max = [],
                            inner_min = [];
                        data.forEach(function(d,i){
                            inner_max.push(d3.max(d));
                            inner_min.push(d3.min(d));
                        })
                        return [d3.min(inner_min), d3.max(inner_max)];
                    }

                    function selectData(p) {
                        var ind = selected_coeffs.indexOf(p.z);
                        if(ind == -1) {
                            selected_coeffs.unshift(p.z);
                            selected_data.unshift(data_obj[p.z]);
                        }
                        else {
                            selected_coeffs.splice(ind,1);
                            selected_data.splice(ind,1);
                        }
                        Session.set("selected_data", selected_data);
                        Session.set("selected_coeffs", selected_coeffs);

                        refreshGraph(true);
                    }

                    /** select a set of coefficients*/
                    function selectDataSet( list ){
                        if (list.length > 0) {
                            selected_data = _.map(list,function(name) {return data_obj[name];});
                        }
                        else {
                            selected_data = list;
                        }
                        selected_coeffs = _.map(list, function(ind) {return parseInt(ind);});
                    }

                    /** find the n coefficients with the strongest changes */
                    function maxChanges( n ){
                        var max = {},
                            min = {};
                        for (d in data_obj) {
                            max[d] = d3.max(data_obj[d]);
                            min[d] = d3.min(data_obj[d]);
                        };
                        max = _.pairs(max);
                        min = _.pairs(min);
                        max.sort(function(a,b) { return (Math.abs(a[1]) > Math.abs(b[1])) ? -1 : 1 });
                        min.sort(function(a,b) { return (Math.abs(a[1]) > Math.abs(b[1])) ? -1 : 1 });

                        max = _.map(max,function(d) {return d[0];}).slice(0,n);
                        min = _.map(min,function(d) {return d[0];}).slice(0,n);
                        return _.union(max,min)
                    }

                    function mouseover(p) {
                        d3.selectAll(".row text").classed("active", function(d, i) { return i == p.y; });
                        d3.selectAll(".column text").classed("active", function(d, i) { return i == p.x; });
                        d3.select(this).style("stroke", "steelblue");
                        // add data to the timecourse object
                        selected_data.push(data_obj[p.z]);

                        //show some info about the field
                        div.transition()
                            .duration(200)
                            .style("opacity", .7);
                        div .html(y_lab[p.y]+"<br>"+x_lab[p.x])
                            .style("left", (d3.event.pageX) + "px")     
                            .style("top", (d3.event.pageY - 28) + "px");
                        refreshGraph(false);
                    }

                    function mouseout(p) {
                        d3.selectAll("text").classed("active", false);
                        d3.select(this).style("stroke", "lightgrey");
                        selected_data.pop();
                        div.transition()
                            .duration(500)
                            .style("opacity", 0);
                        refreshGraph(false);
                    }

                    var row = svg.selectAll(".row")
                            .data(matrix)
                            .enter().append("g")
                            .attr("class", "row")
                            .attr("transform", function(d, i) { return "translate(0," + y(i) + ")"; })
                            .each(create_row);

                    row.append("text")
                        .attr("x", -6)
                        .attr("y", x.rangeBand() / 2)
                        .attr("dy", ".32em")
                        .attr("text-anchor", "end")
                        .text(function(d, i) { return y_lab[i]; });

                    var column = svg.selectAll(".column")
                            .data(matrix[0])
                            .enter().append("g")
                            .attr("class", "column")
                            .attr("transform", function(d, i) { return "translate(" + x(i) + ",0)rotate(-90)"; });

                    column.append("text")
                        .attr("x", 6)
                        .attr("y", x.rangeBand() / 2)
                        .attr("dy", ".32em")
                        .attr("text-anchor", "start")
                        .text(function(d, i) { return x_lab[i]; });

                    var div = d3.select("body").append("div")   
                            .attr("class", "tooltip")
                            .style("opacity", 0);

                    // define dimensions of graph
                    var m = [80, 80, 80, 80]; // margins
                    var w = 720 - m[1] - m[3]; // width
                    var h = 400 - m[0] - m[2]; // height
                    
                    // get data for first coeff that we'll plot with a line (it contains only the Y values, X is the index location)
                    data = data[0];

                    // X scale will fit all values from data[] within pixels 0-w
                    var x_lp = d3.scale.linear();
                    
                    try {
                        x_lp.domain([0, time[time.length-1]])
                            .range([0, w]);                        
                    }
                    catch(err) {
                        
                    }
                    // Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
                    var y_lp = d3.scale.linear();
                    
                    y_lp.domain([d3.min(data), d3.max(data)])
                        .range([h, 0]);

                    // create a line function that can convert data[] into x and y points
                    var line = d3.svg.line()
                            .x(function(d,i) { return x_lp((i/time.length)*time[time.length-1]); })
                            .y(function(d) { return y_lp(d); });

                    // Add an SVG element with the desired dimensions and margin.
                    var graph = d3.select("#plot")
                            .attr("width", w + m[1] + m[3])
                            .attr("height", h + m[0] + m[2])
                            .append("g")
                            .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

                    // create xAxis
                    var xAxis = d3.svg.axis()
                            .scale(x_lp)
                            .tickSize(-h)
                            .tickSubdivide(true);

                    // Add the x-axis.
                    graph.append("g")
                        .attr("class", "x axis")
                        .attr("transform", "translate(0," + h + ")")
                        .call(xAxis)
                        .append("text")
                        .attr("y", 40)
                        .attr("x", 250)
                        .text("Time");

                    // for the export the style needs to be in here:
                    d3.select(".x.axis path")
                        .style("display","none");

                    // create left yAxis
                    var yAxisLeft = d3.svg.axis().scale(y_lp).ticks(5).orient("left");
                    // Add the y-axis to the left
                    graph.append("g")
                        .attr("class", "y axis")
                        .attr("transform", "translate(-25,0)")
                        .call(yAxisLeft);

                    refreshGraph(true);
                    // Add the line by appending an svg:path element with the data line we created above
                    // do this AFTER the axes above so that the line is above the tick-lines

                    //reset the simulate button
                    //$('#btn_simulate').button('reset');
                    $('#btn_simulate').html('Simulate');
                    $('#btn_simulate').prop('disabled', true);
                    $('#input_time').prop('disabled', false);

                    $('#btn_select_all').click(function(){
                        selectDataSet(_.range(x_lab.length*y_lab.length));
                        refreshGraph(true);
                        Session.set("selected_data", selected_data);
                        Session.set("selected_coeffs", selected_coeffs);
                    });
                    //put select functionality on selection button
                    $('#btn_select_min_max').click(function(){
                        selectDataSet(maxChanges(5));
                        refreshGraph(true);
                        Session.set("selected_data", selected_data);
                        Session.set("selected_coeffs", selected_coeffs);
                    });

                    $('#btn_clear_selection').click(function() {
                        selectDataSet([]);
                        refreshGraph(true);
                    });

                    $('#btn_clear_selection').tooltip();
                    $('#btn_select_min_max').tooltip();
                    $('#btn_select_all').tooltip();
                    

                    d3.select("#export_data").append("a")
                        .attr("id","download_link")
                        .attr("title", selected_analysis+"_normalization_"+Session.get("normalization")+".svg")
                        .attr("href-lang", "image/svg+xml")
                        .text("Download .svg");
//                        .on("click", refresh_link);

                }
            }
        });
    }
};