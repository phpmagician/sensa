Template.d3graphics.events({
    //toggles between normalized and non-normalized data
    'click #btn_normalize': function (event) {
        Session.set("normalization", true);
    },
    'click #btn_non_normalize': function (event) {
        Session.set("normalization", false);
    }
});

Template.d3graphics.show = function () {
    return (Session.get("selected_analysis") !== 'get_steady_state') ? true : false;
};

Template.d3graphics.rendered = function () {
    var self = this;

    // init the button and remember state for different coeffs
    if (Session.get("normalization")) { 
        $('#btn_normalize').addClass("active");
    } else { 
        $('#btn_non_normalize').addClass("active");
    }

    if (! self.handle) {
        self.handle = Deps.autorun(function () {
            var selected_model = Session.get('selected_model');
            var selected_analysis = Session.get('selected_analysis');
            
            var fields =  {};
            fields[selected_analysis] = 1;
            var current_analysis = Analysis.findOne({model_id: Session.get('selected_model')}, 
                                                    {fields: fields});
            data = [];
            var svg = d3.select("#matrix").selectAll("g").remove();
            var svg = d3.select("#matrix").selectAll("rect").remove();
            var svg = d3.select("#matrix").selectAll("text").remove();
            var div = d3.select("#export_data").selectAll("a").remove();
            var div = d3.selectAll(".tooltip").remove();
            var svg = d3.select("#selectionMatrix").selectAll("g").remove();
            
            if(current_analysis) {
                try{
                    if (Session.get("normalization") && selected_analysis !== 'get_steady_state') { // false per default, hence here normalized data
                        var data = current_analysis[selected_analysis].data_normalize;

                    } else { // true per default, hence here non-normalized data
                        var data = current_analysis[selected_analysis].data;
                    }

                    var x_lab = current_analysis[selected_analysis].x_axis,
                        y_lab = current_analysis[selected_analysis].y_axis;

                    var n = data.length,
                        m = data[0].length;
                }
                catch(e){
                    data = [];
                }
                
                if(data.length > 0) {
                    //set the matrix to a suitable size
                    var max_width = 720,
                        max_height = 720;

                    // calc margin.top and .left according to max length of labels
                    var max_len_x = d3.max(x_lab.map(function(string) {
                        return string.length;
                    }));
                    var max_len_y = d3.max(y_lab.map(function(string) {
                        return string.length;
                    }));

                    // var margin = {top: 80, right: 80, bottom: 10, left: 80};
                    var margin = {top: Math.max(80, max_len_y*9), right: 80, bottom: 10, left: Math.max(80, max_len_x*9)};

                    if (max_width/m < 20 || max_height/n < 20) {
                        var width = Math.min(max_width*(m/n), max_width),
                            height = Math.min(max_height*(n/m), max_height);                    
                    }
                    else {
                        var width = Math.min(max_width, 20*m),
                            height = Math.min(max_height, 20*n);
                    };

                    // return the max and min of data-matrix (nested array of arrays) - use to set color scale
                    var max = d3.max(data, function(array) {
                        return d3.max(array);
                    });
                    var min = d3.min(data, function(array) {
                        return d3.min(array);
                    });

                    //scales
                    var x = d3.scale.ordinal().rangeBands([0, width]),
                        y = d3.scale.ordinal().rangeBands([0, height]),
                        z = d3.scale.linear().domain([0, 4]).clamp(true),
                        color = d3.scale.linear().domain([min, 0, max]).range(["red", "white", "green"]);
                    
                    var svg = d3.select("#matrix")
                            .attr("width", width + 1 + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    var matrix = [];
                    
                    data.forEach(function(row,i) {
                        matrix[i] = d3.range(m).map(function(j) { return {x: j,
                                                              y: i,
                                                              z: data[i][j]}; });
                    });

                    // Precompute the orders.
                    var xOrders = {
                        // name: d3.range(m).sort(function(a, b) { return (a,b); }),
                        name: d3.range(m),
                    };
                    var yOrders = {
                        // name: d3.range(n).sort(function(a, b) { return (a,b); }),
                        name: d3.range(n),
                    };

                    // The default sort order.
                    x.domain(xOrders.name);
                    y.domain(yOrders.name);

                    svg.append("rect")
                        .attr("class", "background")
                        .attr("width", width)
                        .attr("height", height);

                    function make_row(row) {
                        var cell = d3.select(this).selectAll(".cell")
                                .data(row);
                        cell.enter().append("rect")
                            .attr("class", "cell")
                            .attr("x", function(d) { return x(d.x); })
                            .attr("width", x.rangeBand())
                            .attr("height", x.rangeBand())
                            .style("fill-opacity", 0.4)
                            .style("fill", function(d) { return color(d.z) })
                            .on("mouseover", mouseover)
                            .on("mouseout", mouseout);
                        cell.exit().remove();
                    }
                    function mouseover(p) {
                        d3.selectAll(".row text").classed("active", function(d, i) { return i == p.y; });
                        d3.selectAll(".column text").classed("active", function(d, i) { return i == p.x; });
                        d3.select(this).style("fill-opacity", 1);
                        div.transition()
                            .duration(200)
                            .style("opacity", .7);
                        div .html(x_lab[p.y]+"<br>"+y_lab[p.x]+"<br><br>"+d3.round(p.z,5))
                            .style("left", (d3.event.pageX) + "px")     
                            .style("top", (d3.event.pageY - 28) + "px");
                    }
                    function mouseout() {
                        d3.selectAll("text").classed("active", false);
                        d3.select(this).style("fill-opacity", 0.4);
                        div.transition()
                            .duration(500)
                            .style("opacity", 0); 
                    };

                    var row = svg.selectAll(".row")
                            .data(matrix)
                            .enter().append("g")
                            .attr("class", "row")
                            .attr("transform", function(d, i) { return "translate(0," + y(i) + ")"; })
                            .each(make_row);
                    
                    row.append("line")
                        .attr("x2", width);

                    row.append("line")
                        .attr("y1", x.rangeBand())
                        .attr("y2", x.rangeBand())
                        .attr("x2",width);

                    row.append("text")
                        .attr("x", -6)
                        .attr("y", x.rangeBand() / 2)
                        .attr("dy", ".32em")
                        .attr("text-anchor", "end")
                        .text(function(d, i) { return x_lab[i]; });

                    var column = svg.selectAll(".column")
                            .data(matrix[0])
                            .enter().append("g")
                            .attr("class", "column")
                            .attr("transform", function(d, i) { return "translate(" + x(i) + ",0)rotate(-90)"; });

                    column.append("line")
                        .attr("x1", -height);

                    column.append("line")
                        .attr("y1", x.rangeBand())
                        .attr("y2", x.rangeBand())
                        .attr("x1", -height);

                    column.append("text")
                        .attr("x", 6)
                        .attr("y", x.rangeBand() / 2)
                        .attr("dy", ".32em")
                        .attr("text-anchor", "start")
                        .text(function(d, i) { return y_lab[i]; });

                    var div = d3.select("body").append("div")   
                            .attr("class", "tooltip matrix")
                            .style("opacity", 0);

                    var gradient1 = svg.append("svg:defs")
                          .append("svg:linearGradient")
                            .attr("id", "gradient1")
                            .attr("x1", "50%")
                            .attr("y1", "0%")
                            .attr("x2", "50%")
                            .attr("y2", "100%")
                            .attr("spreadMethod", "pad");

                    gradient1.append("svg:stop")
                            .attr("offset", "0%")
                            .attr("stop-color", "green")
                            .attr("stop-opacity", 0.4);

                    gradient1.append("svg:stop")
                            .attr("offset", "100%")
                            .attr("stop-color", "white")
                            .attr("stop-opacity", 0.4);

                    d3.select("#matrix").append("rect")
                            .attr("class","legend1")
                            .attr("x", width + 1 + margin.left + 10)
                            .attr("y", height + margin.top - 100) // starts at total svg height - own height and - other rect height
                            .attr("width", 20)
                            .attr("height", 50)
                            .style("fill", "url(#gradient1)");

                    var gradient2 = svg.append("svg:defs")
                          .append("svg:linearGradient")
                            .attr("id", "gradient2")
                            .attr("x1", "50%")
                            .attr("y1", "0%")
                            .attr("x2", "50%")
                            .attr("y2", "100%")
                            .attr("spreadMethod", "pad");

                    gradient2.append("svg:stop")
                            .attr("offset", "0%")
                            .attr("stop-color", "white")
                            .attr("stop-opacity", 0.4);

                    gradient2.append("svg:stop")
                            .attr("offset", "100%")
                            .attr("stop-color", "red")
                            .attr("stop-opacity", 0.4);

                    d3.select("#matrix").append("rect")
                            .attr("class","legend2")
                            .attr("x", width + 1 + margin.left + 10)
                            .attr("y", height + margin.top - 50 ) // starts at total svg height - own height
                            .attr("width", 20)
                            .attr("height", 50)
                            .style("fill", "url(#gradient2)")

                    d3.select("#matrix").append("text")
                            .attr("x", width + 1 + margin.left + 31)
                            .attr("y", height + margin.top)
                            .attr("text-anchor", "start")
                            .text(function(d){
                                if (min<0) {return d3.round(min,3)}
                                else {return 0}
                            });

                    d3.select("#matrix").append("text")
                            .attr("x", width + 1 + margin.left + 31)
                            .attr("y", height + margin.top - 45)
                            .attr("text-anchor", "start")
                            .text("0");

                    d3.select("#matrix").append("text")
                            .attr("x", width + 1 + margin.left + 31)
                            .attr("y", height + margin.top - 90)
                            .attr("text-anchor", "start")
                            .text(function(d){
                                if (max>0) {return d3.round(max,3)}
                                else {return 0}
                            });

                    d3.select("#export_data").append("a")
                        .attr("id","download_link")
                        .attr("title", selected_analysis+"_normalization_"+Session.get("normalization")+".svg")
                        .attr("href-lang", "image/svg+xml")
                        .text("Download \.svg");


                    function refresh_link () {
                        var html = d3.select("#matrix")
                            .attr("version", 1.1)
                            .attr("xmlns", "http://www.w3.org/2000/svg")
                            .node().parentNode.innerHTML;
                        d3.select("#download_link")
                            .attr("href", "data:image/svg+xml;base64,\n" + btoa(html))
                            .attr("download", selected_analysis+"_normalization_"+Session.get("normalization")+".svg");
                    }
                    refresh_link();
                };
            };     
        });
    }
};